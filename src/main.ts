import { Group, updateGroupSelector, addExpenseToGroup } from "./basics/Group";

const groups: Group[] = [];
const groupSelector = document.getElementById("group-selector") as HTMLSelectElement;
const expenseNameInput = document.getElementById("expense-name") as HTMLInputElement;
const expenseAmountInput = document.getElementById("expense-amount") as HTMLInputElement;
const paidByInput = document.getElementById("paid-by") as HTMLInputElement;
const expensesList = document.getElementById("expenses-list") as HTMLUListElement;
const totalExpenses = document.getElementById("total-expenses") as HTMLSpanElement;


document.getElementById("create-group")?.addEventListener("click", () => {
  const groupName = (document.getElementById("group-name") as HTMLInputElement).value;
  if (groupName) {
    groups.push({ name: groupName, expenses: [] });
    updateGroupSelector(groups, groupSelector);
  }
});

document.getElementById("add-expense")?.addEventListener("click", () => {
  addExpenseToGroup(
    groups,
    groupSelector,
    expenseNameInput,
    expenseAmountInput,
    paidByInput,
    expensesList,
    totalExpenses
  )
})


updateGroupSelector(groups, groupSelector);
addExpenseToGroup(groups, groupSelector, expenseNameInput, expenseAmountInput, paidByInput, expensesList, totalExpenses);

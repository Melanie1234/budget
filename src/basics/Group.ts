export interface Expense {
    name: string;
    amount: number;
    paidBy: string;
}

export class Group {
    name: string;
    expenses: Expense[];
    constructor(name: string) {
        this.name = name;
        this.expenses = [];
    }
}

export function updateGroupSelector(groups: Group[], groupSelector: HTMLSelectElement) {
    groupSelector.innerHTML = "";
    groups.forEach((group, index) => {
        const option = document.createElement("option");
        option.value = index.toString();
        option.textContent = group.name;
        groupSelector.appendChild(option);
    });
}

export function addExpenseToGroup(
    groups: Group[],
    groupSelector: HTMLSelectElement,
    expenseNameInput: HTMLInputElement,
    expenseAmountInput: HTMLInputElement,
    paidByInput: HTMLInputElement,
    expensesList: HTMLUListElement,
    totalExpenses: HTMLSpanElement
) {
    const selectedGroupIndex = groupSelector.value;
    const name = expenseNameInput.value;
    const amount = parseFloat(expenseAmountInput.value);
    const paidBy = paidByInput.value;

    if (selectedGroupIndex && name && !isNaN(amount) && paidBy) {
        const group = groups[parseInt(selectedGroupIndex)];
        group.expenses.push({ name, amount, paidBy });
        displayGroup(group, expensesList, totalExpenses);
    }
}

function displayGroup(group: Group, expensesList: HTMLUListElement, totalExpenses: HTMLSpanElement) {
    expensesList.innerHTML = ""; 

    let groupTotal = 0;

    for (const expense of group.expenses) {
        const listItem = document.createElement("li");
        listItem.textContent = `${expense.name} - ${expense.amount} € - ${expense.paidBy}`;
        expensesList.appendChild(listItem);

        groupTotal += expense.amount;
    }

    totalExpenses.textContent = groupTotal.toFixed(2);
}
